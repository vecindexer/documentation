Базовые требования
==================

Минимальные требования для работы библиотеки.


Для установки компонентов библиотеки требуется следующее:

* версия python - 3.9, не менее
* утилита pip для python3
* утилита git
* средство создания виртуального окружения для python: venv или virtualenv
* утилита curl


Для работы библиотеки требуется следующее:

* установленный и запущенный один инстанс `vecindexercpp` (`инструкция по установке vecindexercpp доступна здесь <https://exactusvectorindex.readthedocs.io/ru/latest/vecindexercpp.html>`_)
* установленный и готовый к работе `vecindexerclient` (`инструкция по установке vecindexerclient доступна здесь <https://exactusvectorindex.readthedocs.io/ru/latest/vecindexerclient.html>`_)
* установленный и запущенный инстанс `consul` (`инструкция по установке consul достуна здесь <https://exactusvectorindex.readthedocs.io/ru/latest/vecindexercpp.html#how-to-install-consul>`_)


Для использования GPU требуется установленная библиотека CUDA версии 11.
Для установки CUDA см. `официальный сайт <https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html>`_
