

Серверная часть (vecindexercpp)
=============

Описание
--------

Данный проект является шардированной и реплицируемой оберткой библиотеки ``faiss``.
``vecindexercpp`` использует `consul <https://www.consul.io/>`_ для эклемпляров ``health-checks``\ , реализации паттерна ``leader-follower`` и ``service registration``.
``vecindexercpp`` является серверной частью `ExactusVectorIndex <https://gitlab.com/vecindexer/exactusvectorindex>`_.
Для пользователей доступен `vecindexerclient <https://gitlab.com/vecindexer/vecindexerclient>`_, написанный на Python.

Установка
---------

Использование docker образа
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Находится в стадии разработки!

Из источника
^^^^^^^^^^^^

Скачивание репозитория
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   $ git clone https://gitlab.com/vecindexer/vecindexercpp.git 
   $ cd vecindexercpp
   $ git submodule update --init

Сборка (в nix env)
~~~~~~~~~~~~~~~~~~


Использование nix для создания среды разработки.
Преимуществом nix, кроме быстрой установки всех зависимостей, также является возможность использования свежих компиляторов.


* Требуется установка nix в режиме `Single-User Mode <https://nixos.wiki/wiki/Nix_Installation_Guide>`_ версии 2.8 или выше.
* Необходимо добавить конфигурационный файл ``~/.config/nix/nix.conf``:
       ::

               experimental-features = nix-command flakes
               extra-substituters =  https://cuda-maintainers.cachix.org
               trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E=

* Для того, чтобы попасть в окружение с установленными зависимостями нужно в корневой папке проекта выполнить ``nix develop``. 


* Опциональный шаг: настроить окружение для CUDA (актуально, если в системе установлена CUDA)

.. code-block:: bash

  $ bash setup_cuda_paths.sh


* Вход в среду разработки nix и сборка 

.. code-block:: bash

  $ nix develop
  $ mkdir -p build && cd build && cmake ../ && make -j8 && cd ../


Запуск инстанса (с конфигурационным файлом etc/config/test_server.yml )
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Проверить порт инстанса в конфигурационном файле ``etc/config/test_server.yml``, который потом понадобится в ``vecindexerclient``.

Перед запуском требуется:
  - заменить путь параметр ``primary_path`` (из раздела ``storage_config``) на актуальный путь;
  - или создать папку ``/data/test_vecindexer_1_1``.

``primary_path`` -- это папка, в которой хранятся индексы (сохраняются при выключении туда, оттуда же загружаются во время работы).

.. code-block:: bash

   $ nix develop
   $ ./build/src/vec_indexer -c etc/config/test_server.yml -l etc/config/log_config.conf


Если во время запуска инстанса возникла ошибка ``Segmentation fault`` , то нужно заново запустить инстанс.


Остановка инстанса
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Для этого используется комбинация клавиш ``CTRL + C``


.. _how_to_install_consul:

How to install consul (+cluster)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Официальные инструкции по установке <https://developer.hashicorp.com/consul/downloads>`_

.. collapse:: Обход ошибок, возникающих при установке

    .. collapse:: "This content is not currently available in your region"

        If ``https://apt.releases.hashicorp.com/gpg`` shows ``This content is not currently available in your region``.
        Or ``wget`` returns ``code 404``.

        Возможна установка через tor

        .. code-block:: bash

            $ sudo apt install tor
            $ curl -s --socks5 localhost:9050 https://apt.releases.hashicorp.com/gpg -o hashicorp_apt_gpg.txt
            $ cat hashicorp_apt_gpg.txt | gpg --import
            ## copy "KEY" from the output!
            $ gpg --export -a "KEY" | sudo apt-key add - # example: $ gpg --export -a "AA16FCBCA621E701" | sudo apt-key add -
            $ gpg --export -a "KEY" | gpg --dearmor  | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
            $ echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] tor+https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
            $ sudo apt install apt-transport-tor
            $ sudo apt update && sudo apt install consul
            $ consul --version 
            ## Should show version, revision and etc.


Для локального использования:


.. code-block:: bash

   $ mkdir -p /tmp/consul && consul agent -data-dir=/tmp/consul -dev


Для создания кластера:

* Необходимо 1/3/5 consul-servers
* Consul-agent нужен н каждом узле, где будет запущен ``vecindexercpp``


Запуск тестов
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Для запуска тестов необходим запущенный локальный consul, см. `how_to_install_consul`_.

Запуск локального consul-а (в отдельной вкладке, окружение ``nix develop`` не требуется, если consul был установлен при помощи стандартного пакетного менеджера):

.. code-block:: bash

   $ mkdir -p /tmp/consul && consul agent -data-dir=/tmp/consul -dev


Запуск тестов (команда выполняется из папки проекта, для запуска требуется окружение ``nix develop``):

.. code-block:: bash

   $ nix develop
   $ cd build && ctest -j3 --no-compress-output -T Test && cd ../
