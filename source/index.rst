.. exactusvectorindex documentation master file, created by
   sphinx-quickstart on Fri May 12 12:19:52 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Добро пожаловать в документацию ExactusVectorIndex!
====================================================


Библиотека `ExactusVectorIndex <https://gitlab.com/vecindexer/exactusvectorindex>`_ для распределенной индексации
и поиска в сверхбольших массивах векторных представлений (эмбеддингов)
с применением машинного обучения для преобразования векторных пространств и выборки данных.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   requirements
   vecindexercpp
   vecindexerclient
   quick_start


Благодарности
""""""""""""""""
Библиотека создана при поддержке Фонда содействия инновациям (Договор № 12ГУКодИИС12-D7/72692 о предоставлении гранта на выполнение проекта открытых библиотек от 27 декабря 2021 г).
