

Клиентская часть (vecindexerclient)
===================================

Vec Indexer Client (python).  

Python-client для взаимодействия с кластером ``vecindexercpp``.  


Установка
---------

.. code-block:: bash

   $ git clone https://gitlab.com/vecindexer/vecindexerclient.git
   $ cd vecindexerclient
   $ python -m venv .venv # или python -m virtualenv .venv
   $ source .venv/bin/activate
   (.venv) $ git clone https://github.com/dvzubarev/pycbor_extra.git && cd pycbor_extra && pip install . && cd ../
   (.venv) $ git clone https://github.com/Astromis/autoencoders.git
   (.venv) $ cd autoencoders && git clone https://github.com/leymir/hyperbolic-image-embeddings.git && cd hyperbolic-image-embeddings && pip install . && cd ../ && pip install . && cd ../
   (.venv) $ pip install .


Начало работы
-------------

В этом разделе показаны примеры использования библиотеки:


* из командной строки
* из Python-а

Конфигурация кластера
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Конфигурация кластера с одним инстансом ``vecindexercpp``, например, выглядит следующим образом (ниже приведено содержимое файла `etc/config/my_test_cluster_state.ini`):

.. code-block:: ini

   [test_vecindexer.shard.1.replica.1]
   name=test_vecindexer
   tags=cluster,shard_1,leader
   address=0.0.0.0
   port=21011


Требования для запуска примеров
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Для использования клиента требуется запущенный инстанс ``vecindexercpp`` и запущенный локальный consul

Также см. `раздел быстрый старт <https://exactusvectorindex.readthedocs.io/ru/latest/index.html#quick-start>`_


Для примеров на Python-е
""""""""""""""""""""""""

Импорт и настройка конфигурации

.. code-block:: python


   import numpy as np
   from vecindexerclient.vecindexer import VecIndexer
   from vecindexerclient.vecindexer import ShardingStrategy
   from vecindexerclient.vecindexer import ClusterState
   from vecindexerclient.cli import ClientOpts

   dimension = 768
   coll_id = 1111
   estimated_vecs_count=100000
   index = 'IDMap,HNSW32'
   cluster_state_file = 'etc/config/my_test_cluster_state.ini'


Создание объекта vecindexer

.. code-block:: python

   opts = ClientOpts()
   opts.verbose = False
   opts.cluster_state_file = cluster_state_file

   vecindexer = VecIndexer(
       opts,
       ClusterState(opts.cluster_state_file),
       ShardingStrategy(),
   )



Если в кластере присутствует несколько инстансов ``vecindexercpp``\ , то нужно в конфигурацию нужно добавить все инстансы.

.. collapse:: Пример конфига (который будет использован далее)

   .. code-block:: ini

      [vecindexer.shard.1.replica.2]
      name=vecindexer
      tags=cluster,shard_1,leader
      address=localhost
      port=21012

      [vecindexer.shard.2.replica.1]
      name=vecindexer
      tags=cluster,shard_2,leader
      address=localhost
      port=21021

      [vecindexer.shard.2.replica.3]
      name=vecindexer
      tags=cluster,shard_2,follower
      address=localhost
      port=21023

      [vecindexer.shard.3.replica.3]
      name=vecindexer
      tags=cluster,shard_3,leader
      address=localhost
      port=21033



Для примеров из командной строки будут использованы следующие наборы данных
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Генерирование примеров (тестовые эмбеддинги)

.. code-block:: bash

   $ bash utils/generate_test_embs.sh


* ``test_data`` (vectors cnt: 10000, dimension: 256, batch_size: 1000)

  * базовые эмбеддинги, которые хранятся в директории: test_data/embs
  * небольшой набор векторов (подмножество базовых эмбеддингов): test_data/sample_embs.npz

.. code-block:: bash

 $ ls test_data/
 embs  sample_embs.npz
 $ ls test_data/embs/
 batch_0.npz  batch_1.npz  batch_2.npz  batch_3.npz  batch_4.npz  batch_5.npz  batch_6.npz  batch_7.npz  batch_8.npz  batch_9.npz

* ``test_small`` (vectors cnt: 10000, dimension: 128, batch_size: 10000)

  * базовые эмбеддинги, которые хранятся в директории: test_small/embs
  * небольшой набор векторов (подмножество базовых эмбеддингов): test_small/sample_embs.npz

.. code-block:: bash

 $ ls test_small/
 embs  sample_embs.npz
 $ ls test_small/embs/
 batch_0.npz


Создание тестового набора данных (Python)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

.. code-block:: python

   add_vecs_cnt = 10000
   search_vecs_cnt = 1000
   dtype='float32'
   add_embs = np.random.rand(add_vecs_cnt, dimension).astype(dtype)
   add_ids = [i for i in range(add_embs.shape[0])] # внимание! стоит использовать уникальные идентификаторы!
   search_embs = np.random.rand(search_vecs_cnt, dimension).astype(dtype)

Создание коллекции
^^^^^^^^^^^^^^^^^^

Командная строка
""""""""""""""""

..

   Создание коллекции (пример 1)

   .. code-block:: bash

      $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini create_coll --coll_id 1234 --index "IDMap,HNSW32" --dimension 256 --estimated_vecs_count 100000

   .. collapse:: Пример ответа (коллекция успешно создана)

      .. code-block:: bash

         {'req_no': '...req_no...', 'result': {'copies': 1, 'msg': 'OK'}}
         {'req_no': '...req_no...', 'result': {'copies': 2, 'msg': 'OK', 'success_replicas': ['vecindexer.shard.2.replica.3.level.shard']}}
         {'req_no': '...req_no...', 'result': {'copies': 1, 'msg': 'OK'}}


   .. collapse:: Пример ответа (коллекция уже существует)

      .. code-block:: bash

         Collection already exists. Nothing to do. ... .
         Whole result: None



Описание основных параметров:

* coll_id - номер коллекции (целое положительное число)
* index - строка с описанием типа индекса для `faiss index-factory <https://github.com/facebookresearch/faiss/wiki/The-index-factory>`_
* dimension - размерность вектора
* estimated_vecs_count - приблизительное ожидаемое кол-во векторов в коллекции
* use_gpu - использовать GPU (подходит для некоторых типов индексов, см. `Faiss on the GPU <https://github.com/facebookresearch/faiss/wiki/Faiss-on-the-GPU>`_\ )

При указании параметра `use_gpu` индекс перемещается на GPU, если выполняются следующие требования:

* в системе присутствует корректно установенная библиотека CUDA (если нет, тогда возвращается ответ `CUDA is not available`)
* в системе присутствует GPU-устройство (если нет, тогда возвращается ответ `There is GPU devices`)
* в системе присутствует GPU-устройство с достаточным кол-во доступной памяти (если нет, тогда возвращается ответ `There is no memory on GPU`)

Для такого индекса по умолчанию используется тип данных `float16`, чтобы, в частности, поддерживалась квантизация `PQ64x8`.
Пример использования GPU доступен в `examples`.

В `документации faiss указаны типы индексов, поддерживаемых на GPU <https://github.com/facebookresearch/faiss/wiki/Faiss-on-the-GPU#implemented-indexes>`_.

Примеры индекcов, поддерживаемых GPU:

* `IndexFlat` (пример рабочей строки для index-factory: `IDMap,Flat`)
* `IndexIVFFlat` (пример рабочей строки для index-factory: `IDMap,IVF256,Flat`)
* `IndexIVFScalarQuantizer` (пример рабочей строки для index-factory: `IDMap,IVF256,SQ8`)
* `IndexIVFPQ` (пример рабочей строки для index-factory: `IDMap,IVF256,PQ64x8`)

Другие `ограничения по использованию GPU указаны также в документации faiss <https://github.com/facebookresearch/faiss/wiki/Faiss-on-the-GPU#limitations>`_.



``vecindexerclient`` зависимости от кол-ва и размерности векторов может определить, какой способ хранения использовать – тип кодировщика, индекса.
Для обеспечения автоматического индексирования применяется принцип «проверить и предупредить».
``vecindexerclient`` использует следующие данные для проверок:


* пользовательские параметры (размер вектора, оценочное кол-во векторов в коллекции, тип индекса(опционально)) ;
* актуальные данные о вычислительных мощностях кластера ;

При помощи библиотеки `autofaiss <https://github.com/criteo/autofaiss>`_ ``vecindexerclient`` делает следующее:
  * Если ресурсов не хватает, при создании коллекции выводится сообщение об ошибке.
  * Если указать параметр ``--force_create``, тогда под ответственность пользователя будет выполнена попытка создать коллекцию без любой гарантии того, что вектора успешно будут проиндексированы.
  * Если ``index`` не указан, тогда тип индекса будет рекомендован исходя из имеющихся данных.

..

   Создание коллекции (для демонстрации применения энкодера)

   .. code-block:: bash

      $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini create_coll --coll_id 4321 --index "IDMap,HNSW32" --dimension 64 --estimated_vecs_count 100000

   Создание коллекции c обучаемым индексом

   .. code-block:: bash

      $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini create_coll --coll_id 100 --index "IDMap,IVF256,PQ16" --dimension 128 --estimated_vecs_count 10000  --force_create


Python
""""""

.. code-block:: python

   create_params = {
       "coll_id": coll_id,
       "index": index,
       "dimension": dimension,
       "estimated_vecs_count":  estimated_vecs_count,
   }
   create_result = await vecindexer.create(create_params)
   for r in create_result:
      print (r.dump())


.. collapse:: Если возникла ошибка "SyntaxError: 'await' outside function", то поможет следующее:

   .. code-block:: python

      import asyncio
      import uvloop
      asyncio.set_event_loop_policy(uvloop.EventLoopPolicy()) # run once!
      create_result = None
      async def create():
         global create_result
         create_result = await vecindexer.create(create_params)

      loop = asyncio.get_event_loop()
      loop.run_until_complete(create())

      for r in create_result:
         print (r.dump())


Обучение индекса (опциональный шаг, зависит от типа индекса)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

   $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini train --coll_id 100 --train_embs test_small/sample_embs.npz

.. collapse:: Пример успешного ответа

   .. code-block:: bash

      {'req_no': '...req_no...', 'result': {'copies': 1, 'msg': 'OK', 'normalize_L2': False}}
      {'req_no': '...req_no...', 'result': {'copies': 2, 'msg': 'OK', 'normalize_L2': False, 'success_replicas': ['vecindexer.shard.2.replica.3.level.shard']}}
      {'req_no': '...req_no...', 'result': {'copies': 1, 'msg': 'OK', 'normalize_L2': False}}


Добавление векторов
^^^^^^^^^^^^^^^^^^^

Командная строка
""""""""""""""""

..

   Добавление эмбеддингов из директории

   .. code-block:: bash

      $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini add --coll_id 1234 --embs_dir test_data/embs/
      $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini add --coll_id 100 --embs_dir test_small/embs/


   .. collapse:: Пример успешного ответа (1234 - test_data/embs/)

      .. code-block:: bash

         Added test_data/embs/batch_0.npz. Res: ...
         Added test_data/embs/batch_1.npz. Res: ...
         ...
         Added test_data/embs/batch_8.npz. Res: ...
         Added test_data/embs/batch_9.npz. Res: ...

         {'req_no': '...req_no...', 'result': {'copies': 2, 'inserted_vecs': 334, 'msg': 'OK', 'normalize_L2': False, 'success_replicas': ['vecindexer.shard.2.replica.3.level.shard']}}
         {'req_no': '...req_no...', 'result': {'copies': 1, 'inserted_vecs': 333, 'msg': 'OK', 'normalize_L2': False}}
         {'req_no': '...req_no...', 'result': {'copies': 1, 'inserted_vecs': 333, 'msg': 'OK', 'normalize_L2': False}}
         ...

   .. collapse:: Пример успешного ответа (100 - test_small/embs/)

      .. code-block:: bash

         Added test_small/embs/batch_0.npz. Res: ...
         {'req_no': '...req_no...', 'result': {'copies': 2, 'inserted_vecs': 3334, 'msg': 'OK', 'normalize_L2': False, 'success_replicas': ['vecindexer.shard.2.replica.3.level.shard']}}
         {'req_no': '...req_no...', 'result': {'copies': 1, 'inserted_vecs': 3333, 'msg': 'OK', 'normalize_L2': False}}
         {'req_no': '...req_no...', 'result': {'copies': 1, 'inserted_vecs': 3333, 'msg': 'OK', 'normalize_L2': False}}


   Добавление эмбеддингов из директории с применением кодировщика

   .. code-block:: bash

      $ encoder_config="encoder config path" # пример: etc/encoder_configs/dcec_default.yml
      $ out_dir="encoders_directory" 
      $ mkdir -p $out_dir 
      $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini add --coll_id 4321 --embs_dir test_data/embs use_encoder --encoder_config $encoder_config --encoder dcec_default.pt --train_embs test_data/sample_embs.npz --set_l_hidden --encoder_input_dimension 256 --encoder_output_dimension 64 --encoders_dir $out_dir

   .. collapse:: Пример успешного ответа (4321 - test_data/embs/ - с обучением и применением кодировщика)

      .. code-block:: bash

         Applying: 100%...
         Epoch 0: 100%...
         Epoch 1: 100%...
         ...
         Epoch 4: 100%...
         ...
         Applying: 100%...
         Added test_data/embs/batch_0.npz. ...
         Applying: 100%...
         Added test_data/embs/batch_1.npz. ...
         ...
         Applying: 100%...
         Added test_data/embs/batch_8.npz. ...
         Applying: 100%...
         Added test_data/embs/batch_9.npz. ...
         ...
         {'req_no': '...req_no...', 'result': {'copies': 2, 'inserted_vecs': 334, 'msg': 'OK', 'normalize_L2': False, 'success_replicas': ['vecindexer.shard.2.replica.3.level.shard']}}
         {'req_no': '...req_no...', 'result': {'copies': 1, 'inserted_vecs': 333, 'msg': 'OK', 'normalize_L2': False}}
         {'req_no': '...req_no...', 'result': {'copies': 1, 'inserted_vecs': 333, 'msg': 'OK', 'normalize_L2': False}}
         ...

   .. collapse:: Пример успешного ответа (4321 - test_data/embs/ - с применением уже обученного до этого кодировщика)

      .. code-block:: bash

         Applying: 100%...
         Added test_data/embs/batch_0.npz. ...
         Applying: 100%...
         Added test_data/embs/batch_1.npz. ...
         ...
         Applying: 100%...
         Added test_data/embs/batch_8.npz. ...
         Applying: 100%...
         Added test_data/embs/batch_9.npz. ...
         ...
         {'req_no': '...req_no...', 'result': {'copies': 2, 'inserted_vecs': 334, 'msg': 'OK', 'normalize_L2': False, 'success_replicas': ['vecindexer.shard.2.replica.3.level.shard']}}
         {'req_no': '...req_no...', 'result': {'copies': 1, 'inserted_vecs': 333, 'msg': 'OK', 'normalize_L2': False}}
         {'req_no': '...req_no...', 'result': {'copies': 1, 'inserted_vecs': 333, 'msg': 'OK', 'normalize_L2': False}}
         ...


Если кодировщик ещё не был обучен, тогда он обучится на данных из файла, переданного в ``--train_embs``.
Если уже был обучен (если существует файл в ``--out_dir``\ ), тогда загрузится обученная модель из ``--out_dir``.  

Данный способ добавления нужен для того, чтобы индексировать вектора, кодированные энкодером (меньшего размера).

Преимущества:

* Выигрыш времени в связи с снижением затрат на пересылку данных по сети ;
* Вектора меньшего размера быстрее добавляются в индекс и поиск по ним осуществлятся быстрее;

Недостатки:

* В большинстве случаев теряется качество поиска ;

Python
""""""

.. code-block:: python

   add_params = {
       "coll_id": coll_id,
   }
   add_result = await vecindexer.add(keys=add_ids, vecs=add_embs, params=add_params)
   for r in add_result:
      print (r.dump())


.. collapse:: Если возникла ошибка "SyntaxError: 'await' outside function", то поможет следующее:

   .. code-block:: python

      import asyncio
      import uvloop
      #asyncio.set_event_loop_policy(uvloop.EventLoopPolicy()) # run once!
      add_result = None
      async def add():
         global add_result
         add_result = await vecindexer.add(keys=add_ids, vecs=add_embs, params=add_params)

      loop = asyncio.get_event_loop()
      loop.run_until_complete(add())

      for r in add_result:
         print (r.dump())

.. collapse:: Пример успешного ответа (1111 - python)

   .. code-block:: bash

      {'req_no': '...req_no...', 'result': {'copies': 1, 'inserted_vecs': 3334, 'msg': 'OK', 'normalize_L2': False}}
      {'req_no': '...req_no...', 'result': {'copies': 2, 'inserted_vecs': 3333, 'msg': 'OK', 'normalize_L2': False, 'success_replicas': ['vecindexer.shard.2.replica.3.level.shard']}}
      {'req_no': '...req_no...', 'result': {'copies': 1, 'inserted_vecs': 3333, 'msg': 'OK', 'normalize_L2': False}}



Поиск
^^^^^

Командная строка
""""""""""""""""

..

   для каждого файла эмбеддингов из каталога будет проведен независимый поиск

   .. code-block:: bash

      $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini search --coll_id 1234 --embs_dir test_data/embs 
      $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini search --coll_id 100 --embs_dir test_small/embs

   .. collapse:: Пример успешного ответа (1234 - test_data)

      .. code-block:: bash

         Search finished: test_data/embs/batch_0.npz. Res: {'found': 1000, 'found_ids_shape': (1000, 10), 'found_dist_shape': (1000, 10)}
         Search finished: test_data/embs/batch_1.npz. Res: {'found': 1000, 'found_ids_shape': (1000, 10), 'found_dist_shape': (1000, 10)}
         ...
         Search finished: test_data/embs/batch_8.npz. Res: {'found': 1000, 'found_ids_shape': (1000, 10), 'found_dist_shape': (1000, 10)}
         Search finished: test_data/embs/batch_9.npz. Res: {'found': 1000, 'found_ids_shape': (1000, 10), 'found_dist_shape': (1000, 10)}


   .. collapse:: Пример успешного ответа (100 - test_small)

      .. code-block:: bash

         Search finished: test_small/embs/batch_0.npz. Res: {'found': 10000, 'found_ids_shape': (10000, 10), 'found_dist_shape': (10000, 10)}


   поиск по коллекции, в который добавлены вектора после примерения кодировщика

   .. code-block:: bash

      $ encoder_config="encoder config path" # пример: etc/encoder_configs/dcec_default.yml
      $ out_dir="encoders_directory" 
      $ mkdir -p $out_dir  
      $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini search --coll_id 4321 --embs_dir test_data/embs use_encoder --encoder_config $encoder_config --encoder dcec_default.pt --train_embs test_data/sample_embs.npz --set_l_hidden --encoder_input_dimension 256 --encoder_output_dimension 64 --encoders_dir $out_dir


   .. collapse:: Пример успешного ответа (4321 - test_data/embs - с применением уже обученного до этого кодировщика)

      .. code-block:: bash

         Applying: 100%...
         Search finished: test_data/embs/batch_0.npz. Res: {'found': 1000, 'found_ids_shape': (1000, 10), 'found_dist_shape': (1000, 10)}
         Applying: 100%...
         Search finished: test_data/embs/batch_1.npz. Res: {'found': 1000, 'found_ids_shape': (1000, 10), 'found_dist_shape': (1000, 10)}
         ...
         Applying: 100%...
         Search finished: test_data/embs/batch_8.npz. Res: {'found': 1000, 'found_ids_shape': (1000, 10), 'found_dist_shape': (1000, 10)}
         Applying: 100%...
         Search finished: test_data/embs/batch_9.npz. Res: {'found': 1000, 'found_ids_shape': (1000, 10), 'found_dist_shape': (1000, 10)}



Python
""""""

.. code-block:: python

   search_params = {
       "coll_id": coll_id,
   }
   search_result = await vecindexer.search(vecs=search_embs, params=search_params)
   print ("Found vectors:", search_result["found"])
   print ("Found I shape:", search_result["found_ids"].shape)
   print ("Found D shape:", search_result["found_distances"].shape)

.. collapse:: Если возникла ошибка "SyntaxError: 'await' outside function", то поможет следующее:

   .. code-block:: python

      import asyncio
      import uvloop
      #asyncio.set_event_loop_policy(uvloop.EventLoopPolicy()) # run once!
      search_result = None
      async def search():
         global search_result
         search_result = await vecindexer.search(vecs=search_embs, params=search_params)

      loop = asyncio.get_event_loop()
      loop.run_until_complete(search())

      print ("Found vectors:", search_result["found"])
      print ("Found I shape:", search_result["found_ids"].shape)
      print ("Found D shape:", search_result["found_distances"].shape)


.. collapse:: Пример успешного ответа (1111 - python)

   .. code-block:: bash

      Found vectors: 1000
      Found I shape: (1000, 10)
      Found D shape: (1000, 10)


Состояние кластера (для мониторинга и т.д.)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Командная строка
""""""""""""""""

.. code-block:: bash

   $ python vecindexerclient/cli.py -cl etc/config/my_test_cluster_state.ini info

.. collapse:: Пример успешного ответа

   .. code-block:: bash

      {'req_no': '...req_no...', 'result': {'collections_info': [{'coll_id': 100, 'count': 3333, 'dimension': 128, 'trained': True}, {'coll_id': 1111, 'count': 3334, 'dimension': 768, 'trained': True}, {'coll_id': 1234, 'count': 3333, 'dimension': 256, 'trained': True}, {'coll_id': 4321, 'count': 6666, 'dimension': 64, 'trained': True}], 'msg': 'OK', 'sysinfo': {'free_ram_bytes': 356913152, 'omp_threads': 4, 'proc_count': 8, 'total_ram_bytes': 16684720128}}}
      {'req_no': '...req_no...', 'result': {'collections_info': [{'coll_id': 100, 'count': 3334, 'dimension': 128, 'trained': True}, {'coll_id': 1111, 'count': 3333, 'dimension': 768, 'trained': True}, {'coll_id': 1234, 'count': 3334, 'dimension': 256, 'trained': True}, {'coll_id': 4321, 'count': 6668, 'dimension': 64, 'trained': True}], 'msg': 'OK', 'sysinfo': {'free_ram_bytes': 356913152, 'omp_threads': 4, 'proc_count': 8, 'total_ram_bytes': 16684720128}}}
      {'req_no': '...req_no...', 'result': {'collections_info': [{'coll_id': 100, 'count': 3333, 'dimension': 128, 'trained': True}, {'coll_id': 1111, 'count': 3333, 'dimension': 768, 'trained': True}, {'coll_id': 1234, 'count': 3333, 'dimension': 256, 'trained': True}, {'coll_id': 4321, 'count': 6666, 'dimension': 64, 'trained': True}], 'msg': 'OK', 'sysinfo': {'free_ram_bytes': 356913152, 'omp_threads': 4, 'proc_count': 8, 'total_ram_bytes': 16684720128}}}


Python
""""""

.. code-block:: python

   info = await vecindexer.info({})

.. collapse:: Если возникла ошибка "SyntaxError: 'await' outside function", то поможет следующее:

   .. code-block:: python

      import asyncio
      import uvloop
      #asyncio.set_event_loop_policy(uvloop.EventLoopPolicy()) # run once!
      info_result = None
      async def info():
         global info_result
         info_result = await vecindexer.info({})

      loop = asyncio.get_event_loop()
      loop.run_until_complete(info())

      for r in info_result:
         print (r.dump())


Дополнительные заметки (Python)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. collapse:: Настройка asyncio внутри jupyter-notebook-а

   .. code-block:: python

      import asyncio
      asyncio.get_event_loop()

   ..

   Неоходимо убедиться, что вывод содержит running=True!



.. collapse:: Настройка asyncio в .py файле с main-ом

   .. code-block:: python

      import asyncio

      async def main():
          # ДАЛЬНЕЙШИЙ КОД ЗДЕСЬ
          # ...

      if __name__ == "__main__":
          loop = asyncio.get_event_loop()
          loop.run_until_complete(main())
          loop.close()


Best practices
--------------

``vecindexercpp`` и ``faiss`` рекомендуют использовать пакетную обработку данных.

В связи с этим, для поиска и добавления необходимо использовать приемлемые размеры пакетов (не маленький и не большой - несколько сотен МБ).

Например, для векторов размером 1024 -- ``batch_size=100000``.  

В связи с тем, что ``vecindexercpp`` использует библиотеку ``faiss`` для индексации векторов, смотреть `faiss wiki <https://github.com/facebookresearch/faiss/wiki/>`_.  

..

   Если вы оказались здесь, и вам нужна дополнительная информация, то начать смотреть здесь: ``vecindexerclient/cli.py``.


Выполнение тестов
^^^^^^^^^^^^^^^^^

.. code-block:: bash

   (.venv) $ pytest -v
   (.venv) $ pytest -v --log-cli-level=DEBUG # с логами DEBUG
   (.venv) $ pytest -vvv --log-cli-level=DEBUG --durations=0 #с логами DEBUG и продолжительностью тестов
