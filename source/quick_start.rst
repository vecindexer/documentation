Быстрый старт
""""""""""""""""

1. Установить consul (см. раздел "How to install consul" в `vecindexercpp`) и запустить локальный инстанс в отдельном окне/терминале.

   .. code-block:: bash

      $ mkdir -p /tmp/consul && consul agent -data-dir=/tmp/consul -dev

2. Установить серверную часть (см. `vecindexercpp`) и запустить инстанс в отдельном окне/терминале.

   Для запуска будет использована конфигурация `etc/config/test_server.yml`.

   .. collapse:: Содержимое файла `etc/config/test_server.yml`

         .. code-block:: yaml

            version: 0.1

            consul:
              address: http://localhost
              port: 8500
              wait: 5s
              leadership_sleep_ms: 3000
              service_check:

            server_config:
              service_name: test_vecindexer
              shard_name: 1
              replica_name: 1
              address: 0.0.0.0
              port: 21011
              cluster_service_address: 0.0.0.0
              cluster_service_port: 21111
              time_zone: UTC+3

            indexer_config:
              omp_num_threads: 0

            storage_config:
              primary_path: /data/test_vecindexer_1_1

   1.1. Убедиться в наличии папки `/data/test_vecindexer_1_1`

   .. code-block:: bash

      $ mkdir -p /data/test_vecindexer_1_1

   1.2. Запуск инстанса

   .. code-block:: bash

      $ ./build/src/vec_indexer -c etc/config/test_server.yml -l etc/config/log_config.conf


2. Установить клиентскую часть (см. `vecindexerclient`) и работать с библиотекой при помощи клиентской части

   Работа при помощи клиентской части выполняется в отдельном окне/терминале.
   Клиентская часть для соединения с кластером используем конфигурацию кластера `etc/config/my_test_cluster_state.ini` со следующим содержимым

   .. collapse:: Содержимое файла `etc/config/my_test_cluster_state.ini`

      .. code-block:: ini

         [vecindexer.shard.1.replica.1]
         name=vecindexer
         tags=cluster,shard_1,leader
         address=0.0.0.0
         port=21011


   2.1. Запуск базового примера

   .. code-block:: bash

      $ python examples/base_example.py

   .. collapse:: Пример вывода

      .. code-block:: bash

         {'req_no': '49d8e504-0a8b-11ee-b3cc-a8a1599b7ec8', 'result': {'copies': 1, 'msg': 'OK'}}
         Collection created!
         {'req_no': '49d9e364-0a8b-11ee-b3cc-a8a1599b7ec8', 'result': {'copies': 1, 'inserted_vecs': 1000, 'msg': 'OK', 'normalize_L2': False}}
         Vectors added!
         Search finished!
         Found vectors: 100
         Found I shape: (100, 10)
         Found I(one example): [[905 169 849 767 706 362 539 177  65 387]]
         Found D shape: (100, 10)
         Found D(one example): [[14.578655  14.580481  14.582277  14.609537  14.645647  14.67926
           14.698341  14.7162285 14.735726  14.782282 ]]

   2.2. Запуск примера с применением кодировщика

   .. code-block:: bash

      $ python examples/example_with_encoder.py

   .. collapse:: Пример вывода

      .. code-block:: bash

         {'req_no': '85f52a66-0a8b-11ee-be4b-a8a1599b7ec8', 'result': {'copies': 1, 'msg': 'OK'}}
         Collection created!
         Applying: 100%
         Epoch 0:   0%|
         Epoch 0: 100%|
         Epoch 1: 100%|
         Epoch 2: 100%|
         Epoch 3: 100%|
         Epoch 4: 100%|
         Applying: 100%|
         {'req_no': '86720cf2-0a8b-11ee-be4b-a8a1599b7ec8', 'result': {'copies': 1, 'inserted_vecs': 1000, 'msg': 'OK', 'normalize_L2': False}}
         Vectors added!
         Applying: 100%|
         Search finished!
         Found vectors: 100
         Found I shape: (100, 10)
         Found I(one example): [[263 467 914 942 510  67 243 998 319 506]]
         Found D shape: (100, 10)
         Found D(one example): [[7.6153493 7.618742  7.622263  7.632495  7.649375  7.649886  7.66628
           7.668474  7.6786013 7.6887817]]

   2.3. Запуск примера с использованием компонента хранения связей между документами

   .. code-block:: bash

      $ python examples/example_with_multi_level_doc.py

   .. collapse:: Пример вывода

      .. code-block:: bash

         {'req_no': 'e4ffce46-0ab1-11ee-ae61-a8a1599b7ec8', 'result': {'copies': 1, 'msg': 'OK'}}
         Collection created!
         {'req_no': 'e500da02-0ab1-11ee-ae61-a8a1599b7ec8', 'result': {'copies': 1, 'inserted_vecs': 1000, 'msg': 'OK', 'normalize_L2': False}}
         Vectors with multi level id table added!
         Search finished!
         Found vectors: 100
         Found parent documents(shape): (100, 10, 2)
         Found parent documents(one example): [[1596 1193]
          [1670 1341]
          [1724 1449]
          [1617 1235]
          [1638 1278]
          [1625 1251]
          [1721 1443]
          [1687 1376]
          [1650 1301]
          [1617 1236]]
         Found I shape: (100, 10)
         Found I(one example): [[387 683 898 471 556 503 887 752 603 472]]
         Found D shape: (100, 10)
         Found D(one example): [[14.49551  14.52833  14.529878 14.540271 14.557966 14.575214 14.602661
           14.634747 14.649349 14.709127]]

   2.4. Запуск примера с использованием GPU (актуален при наличии GPU и корректно установленной библиотеки CUDA)

   .. code-block:: bash

      $ python examples/example_with_gpu.py

   .. collapse:: Пример вывода (успешный)

      .. code-block:: bash

         {'req_no': '418dc55a-0a8f-11ee-b47c-a8a1599b7ec8', 'result': {'copies': 1, 'msg': 'OK; OK'}}
         Collection created!
         {'req_no': '422c3bc2-0a8f-11ee-b47c-a8a1599b7ec8', 'result': {'copies': 1, 'msg': 'OK', 'normalize_L2': False}}
         Trained!
         {'req_no': '426cadb0-0a8f-11ee-b47c-a8a1599b7ec8', 'result': {'copies': 1, 'inserted_vecs': 1000, 'msg': 'OK', 'normalize_L2': False}}
         Vectors added!
         Search finished!
         Found vectors: 100
         Found I shape: (100, 10)
         Found I(one example): [[421 171 295  49 208 105  87  44  85  48]]
         Found D shape: (100, 10)
         Found D(one example): [[0.0170725  0.01707423 0.01715308 0.01720279 0.0173108  0.01733881
           0.01744378 0.01751089 0.01762432 0.01831782]]

   .. collapse:: Пример вывода (без применения GPU) - данные будут обрабатываться не на GPU

      .. code-block:: bash

         {'req_no': 'ec522b24-0b4e-11ee-a6f3-913035031a55', 'result': {'copies': 1, 'msg': 'OK; CUDA is not available!'}}
         Collection created!
         {'req_no': 'ec522b25-0b4e-11ee-a6f3-913035031a55', 'result': {'copies': 1, 'msg': 'OK', 'normalize_L2': False}}
         Trained!
         {'req_no': '01bf3bc8-0b4f-11ee-a6f3-913035031a55', 'result': {'copies': 1, 'inserted_vecs': 1000, 'msg': 'OK', 'normalize_L2': False}}
         Vectors added!
         Search finished!
         Found vectors: 100
         Found I shape: (100, 10)
         Found I(one example): [[104 470 475  10  78 424 384 132 399 343]]
         Found D shape: (100, 10)
         Found D(one example): [[0.00967526 0.00971529 0.00973088 0.00982583 0.00998673 0.00999981
           0.01013792 0.0101617  0.0101777  0.01046249]]