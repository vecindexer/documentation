# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Библиотека ExactusVectorIndex'
copyright = 'ExactusVectorIndex contributors'
author = 'ExactusVectorIndex contributors'
release = '0.2'
date = '11.05.2023'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx_toolbox.collapse',
    'sphinx.ext.autosectionlabel',
]
autosectionlabel_prefix_document = True

templates_path = ['_templates']
exclude_patterns = []


language = 'ru'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
